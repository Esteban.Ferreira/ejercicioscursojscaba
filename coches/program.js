function datos() {
  let datosIngresados = {
    marca: document.querySelector("#marca").value.toUpperCase(),
    modelo: document.querySelector("#modelo").value.toUpperCase(),
    resultado: document.querySelector("#resultado"),
  };
  /* Se pasan los values a UpperCase para que sea indiferente como lo ingresa el usuario */
  return datosIngresados;
}

function buscarDescuento() {
  const marcaYModelo = datos();
  /* SOLUCION CON IF ELSE
  if ((marcaYModelo.marca == "ford") && (marcaYModelo.modelo == "fiesta")) {
    return marcaYModelo.resultado.innerHTML = "Obtiene un 5% de descuento";
  }
  if ((marcaYModelo.marca == "ford") && (marcaYModelo.modelo == "focus")) {
    return marcaYModelo.resultado.innerHTML = "Obtiene un 10% de descuento";
  }else{
    return marcaYModelo.resultado.innerHTML = "No hay descuento para este modelo";
  }*/

  /* SOLUCION CON SWITCH */
  
  if (marcaYModelo.marca == "FORD"){
    switch (marcaYModelo.modelo) {
      case "FIESTA":
        return marcaYModelo.resultado.innerHTML = "Obtiene un 5% de descuento";
      case "FOCUS":
        return marcaYModelo.resultado.innerHTML = "Obtiene un 10% de descuento";
      default:
        break;
    }
  }else{
    return marcaYModelo.resultado.innerHTML = "No hay descuento para este modelo";
  }

}

function limpiarInputs(){
  document.querySelector("#marca").value="";
  document.querySelector("#modelo").value="";
  document.querySelector("#resultado").value="";
}

function botonBuscar() {
  document
    .querySelector("#search")
    .setAttribute("onclick", "buscarDescuento()");
}

function botonLimpiarCampos() {
  document
    .querySelector("#cleanInputs")
    .setAttribute("onclick", "limpiarInputs()");
}

function main() {
  botonBuscar();
  botonLimpiarCampos();
}

main();
