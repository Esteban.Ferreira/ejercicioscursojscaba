let miVariable = "cualquiercosa";
let miOtraVariable = "MAYUSCULAS";

function pasarAMayusculas (){
    let pasadoAMayúsculas = miVariable.toUpperCase();
    return console.log (pasadoAMayúsculas);
}

function pasarAMinusculas (){
    let pasadoAMinusculas = miOtraVariable.toLowerCase();
    return console.log (pasadoAMinusculas);
}

function main() {

    pasarAMayusculas();
    pasarAMinusculas();

  }
  
  main();
