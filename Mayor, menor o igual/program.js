document.querySelector("#aceptar").setAttribute("onclick", "validarNum()");

let num1 = parseInt(document.querySelector("#num1").value);
let num2 = parseInt(document.querySelector("#num2").value);

function validarNum() {
  if (isNaN(num1) || isNaN(num2)) {
    alert("Solo se pueden ingresar números");
  } else {
    comparar();
  }
}

function comparar() {
  
  let res = "";

  if (num1 > num2) {
    res = "El número 1 es mayor al número 2";
  }
  if (num1 < num2) {
    res = "El número 1 es menor al número 2";
  }
  if (num1 == num2) {
    res = "El número 1 es igual al número 2";
  }
    
  document.querySelector("#resultado").innerHTML = res;
}
