function datos() {
  let datosIngresados = {
    numeroIngresado: parseInt(document.querySelector("#Numero1").value),
    resultado: document.querySelector("#resultado"),
  };
  return datosIngresados;
}

function validar() {
  const datosAux = datos();
  console.log(datosAux);
  if (datosAux.numeroIngresado >= 1 && datosAux.numeroIngresado <= 120) {
    if (datosAux.numeroIngresado >= 18) {
      return datosAux.resultado.innerHTML = "Ingreso validado";
    } else {
      return  datosAux.resultado.innerHTML = "El usuario no es mayor de edad";
    }
  } else {
    return datosAux.resultado.innerHTML =
      "El número ingresado no es válido. Debe ingresar un número en el rango de 1 a 120";
  }
}

function botonValidar() {
  document.querySelector("#validate").setAttribute("onclick", "validar()");
}

function main() {
  botonValidar();
}

main();
