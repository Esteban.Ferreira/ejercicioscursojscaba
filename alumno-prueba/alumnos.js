let alumno = {
  nombre: ["Leandro", "Juan", "Esteban", "Ruben"],
  apellido: ["Villarreal", "Graneros", "Ferreira", "Coronel"],
};

function imprimirAlumnos() {
  let alumnoAux = [];
  for (let i = 0; i < alumno.nombre.length; i++) {
    alumnoAux.push(" " + alumno.nombre[i] + " " + alumno.apellido[i]);
    //Otra forma de resolver la iteración
    //alumnoAux[i] = (" " + alumno.nombre[i] + " " + alumno.apellido[i]);
  }

  return console.log("Los alumnos del curso son:" + alumnoAux);
}

function main() {
  imprimirAlumnos();
}

main();
